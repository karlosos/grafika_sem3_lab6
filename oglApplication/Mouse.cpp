#include "Mouse.h"
#include "3rd\GLFW\include\glfw3.h"


Mouse::Mouse()
{
}

void Mouse::MouseInit()
{
}

void Mouse::MouseKill()
{
}

void Mouse::resetPosition()
{
	glfwSetCursorPos(glfwGetCurrentContext(), 800 / 2, 600 / 2);
}

glm::vec2 Mouse::getPosition()
{
	double x;
	double y;
	glfwGetCursorPos(glfwGetCurrentContext(), &x, &y);
	return glm::vec2(x, y);
}


Mouse::~Mouse()
{
}
