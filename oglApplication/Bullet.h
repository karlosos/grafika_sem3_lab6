#pragma once
class Bullet
{
public:
	Bullet(float x, float y, float rotate);
	~Bullet();
	float x;
	float y;
	float rotate;
	float speed;
	int hops;
	
	void updatePosition();
};

