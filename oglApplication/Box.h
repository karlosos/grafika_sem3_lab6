#pragma once
struct Position {
	float x;
	float y;
	float z;
};

struct Scale {
	float width;
	float height;
	float length;
};

class Box
{
public:
	Box(float x = 0, float y = 0, float z = 0, float width = 0, float height = 0, float length = 0);
	virtual ~Box();
	Position position;
	Scale scale;
	int type;
	bool isCollision(float x, float y);
};

