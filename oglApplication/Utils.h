#pragma once
#ifndef UTILS_H
#define UTILS_H
#include <vector>

int randomInt(int startowa_liczba = 0, int ostatnia_liczba = 100000);
int max(int left, int right);
std::vector<int> generujKlucze(int n, bool czy_losowy);

#endif