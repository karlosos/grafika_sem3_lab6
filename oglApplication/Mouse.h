#pragma once
#include "3rd\glm\glm.hpp"
class Mouse
{
	Mouse();
public:
	static void MouseInit();
	static void MouseKill();
	static void resetPosition();
	static glm::vec2 getPosition();
	~Mouse();
};