#pragma once
#include <random>
#include "Utils.h"

int randomInt(int startowa_liczba, int ostatnia_liczba) {
	return rand() % (ostatnia_liczba - startowa_liczba) + startowa_liczba;
}

int max(int left, int right) {
	return (left<right)?right:left;
}


std::vector<int> generujKlucze(int n, bool czy_losowy) {
	std::vector<int> klucze;
	// losowanie kluczy
	if (czy_losowy == 0) {
		// nie rozklad losowy
		int k = 1;
		for (int i=0; i<n; i++) {
			if (i%2) {
				klucze.push_back(k);
				k++;
			} else {
				klucze.push_back(randomInt());
			}
		}
	} else {
		for (int i=0; i<n; i++) {
			klucze.push_back(randomInt());
		}
	}
	return klucze;
}