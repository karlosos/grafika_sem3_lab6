#include "Bullet.h"
#include "3rd\glm\glm.hpp"


Bullet::Bullet(float x, float y, float rotate)
{
	this->x = x;
	this->y = y;
	this->rotate = rotate;
	speed = 0.5;
	hops = 5;
}


Bullet::~Bullet()
{
}

void Bullet::updatePosition()
{
	x = x + speed * (float)cos(rotate * 3.14 / 180.0f);
	y = y - speed * (float)sin(rotate * 3.14 / 180.0f);
}
