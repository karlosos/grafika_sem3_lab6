#include "Box.h"

Box::Box(float x, float y, float z, float width, float height, float length)
{
	type = 0;
	position = {x,y,z};
	scale = {width,height,length};
}


Box::~Box()
{
}

bool Box::isCollision(float x, float y)
{
	float box_y_max = position.y + (scale.length*30.f) / 2.f + 0.1;
	float box_y_min = position.y - (scale.length*30.f) / 2.f - 0.1;

	float box_x_max = position.x + (scale.width*30.f) / 2.f + 0.1;
	float box_x_min = position.x - (scale.width*30.f) / 2.f - 0.1;

	if (x >= box_x_min && x <= box_x_max) {
		if (y >= box_y_min && y <= box_y_max) {
			return true;
		}
	}
	return false;
}