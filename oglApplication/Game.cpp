
//#define GLM_FORCE_RADIANS

#include "Game.h"
#include "3rd\glm\gtc\matrix_transform.hpp"
#include "3rd\glm\glm.hpp"
#include "Content.h"
#include "Keyboard.h"
#include "Mouse.h"

Game::Game(void)
{
	
}

Game::~Game(void)
{
}

// inicjalizacja parametrow renderingu
void Game::Init()
{
	
	// parametry polozenia i ruchu gracza
	third_person = false;
	cooldown = 0;
	camera_cooldown = 0;
	camera_rotate = 0;
	sceneWidth = 10;
	sceneHeight = 8;
	rotate = -90;
	speed = 0.05f; // predkosc ruchu czajnika
	posX = 0.0f;
	posY = 0.0f;

	theta = 1.0f;
	Effect::ShadersDirectory = "";
	glClearColor(0.6f, 0.5f, 1.0f, 1.0f );

	//stworzenie figury (quada) do wy�wietlania render target�w
	quad = new Quad();

	// wczytanie tekstury
	tex = Content::LoadTexture("../resources/test.png");
	diamond_tex = Content::LoadTexture("../resources/diamond.png");
	samolot_tex = Content::LoadTexture("../resources/bok-fin.png");

	// wczytanie modelu
	testModel = Content::LoadModel( "../resources/teaPot.ASE" );
	diamond = Content::LoadModel("../resources/diament.ASE");
	for (int i = 0; i < diamond->Elements(); i++) {
		//ustawienie koloru materia�u dla pobranego modelu
		diamond->GetMesh(i)->GetMaterial().Color = glm::vec3(0, 1, 0);
		//podmiana tekstury w modelu
		diamond->GetMesh(i)->GetMaterial().texture = diamond_tex;
	}

	samolot = Content::LoadModel("../resources/samolot.ASE");
	for (int i = 0; i < samolot->Elements(); i++) {
		//ustawienie koloru materia�u dla pobranego modelu
		samolot->GetMesh(i)->GetMaterial().Color = glm::vec3(0, 1, 0);
		//podmiana tekstury w modelu
		samolot->GetMesh(i)->GetMaterial().texture = samolot_tex;
	}

	//ustawienie materia�u 
	glm::mat4x4 mm;
	for(int i = 0; i < testModel->Elements(); i++)
	{
		//ustawienie koloru materia�u dla pobranego modelu
		testModel->GetMesh(i)->GetMaterial().Color = glm::vec3(1,1,0);
		//podmiana tekstury w modelu
		testModel->GetMesh(i)->GetMaterial().texture = tex;

		//mm = testModel->GetMesh(i)->getLocalWorld();
	}

	// drugi model (sciany na scenie)
	sceneModelBox = Content::LoadModel("../resources/cube.ASE");
	for(int i = 0; i < sceneModelBox->Elements(); i++) {
		sceneModelBox->GetMesh(i)->GetMaterial().Color = glm::vec3( 0.5f, 0.5f, 0.5f );
	}

	gameBox = Content::LoadModel("../resources/cube.ASE");
	for (int i = 0; i < gameBox->Elements(); i++) {
		gameBox->GetMesh(i)->GetMaterial().Color = glm::vec3(1.f, 0.f, 0.f);
	}

	// dodanie scian
	walls.push_back(new Box(0.f, 90.f, 0.f, 1.f, 0.1f, 0.01f));
	walls.push_back(new Box(0.f, -2.f, 0.f, 1.f, 0.1f, 0.01f));
	walls.push_back(new Box(15.f, 50.f, 0.f, 0.01f, 0.1f, 3.5f));
	walls.push_back(new Box(-15.f, 50.f, 0.f, 0.01f, 0.1f, 3.5f));
	//boxes.push_back(new Box(0,0,0,1,1,1));

	for (int i = -13; i < 13; i++) {
		for (int j = 0; j < 88; j++) {
			if (randomInt(0, 100) <= 20) {
				if (randomInt(0, 100) <= 70) {
					good_boxes.push_back(new Box(i, j, 0, 0.01f, 0.01f, 0.01f));
				}
				else {
					bad_boxes.push_back(new Box(i, j, 0, 0.01f, 0.01f, 0.01f));
				}
			}
		}
	}

	//////////////////////////////////////// RENDER TARGETS
	rtTMO = new RenderTarget2D( 800, 600, GL_RGBA8, GL_RGBA, GL_UNSIGNED_INT, GL_DEPTH24_STENCIL8 );
	rt = new RenderTarget2D( 800, 600, GL_RGBA32F, GL_RGBA, GL_FLOAT, GL_DEPTH24_STENCIL8 );


	/////////////////////////////////////// SHADERS
	// wczytanie i inicjalizacja shadera obliczajacego kolor powierzchni obiektu 
	// wczytuje shadery (wspiera jedynie fragment (.ps) i vertex shadery (.vs), wystarczy poda� nazw� pliku bez rozszerzenia)
	shaderColor= new Effect( "shaders/color" );
	// kompilacja shadera, alteratywnie mo�na wykorzysta� CreateShaderInfo, ktory wypisze w konsoli dane nt. shadera oraz ewnetualne b��dy.
	shaderColor->CreateShader();

	// wczytanie i inicjalizacja shadera obliczajacego mapowanie tonow
	shaderTMO = new Effect( "shaders/tmo" );
	shaderTMO->CreateShader();

	// wczytanie i inicjalizacja shadera renderujacego obraz do framebuffera
	shaderTextureDrawer = new Effect( "shaders/texture" );
	shaderTextureDrawer->CreateShader();


	/////////////////////////////////////// KAMERA
	// ustawienie parametr�w kamery (konstruktor przyjmuje rozdzielczo�� viewportu), 
	// domy�lny field of view to 45*, mo�na zmieni� metod� SetFOV(float)
	camera = Camera( 800, 600 );
	camera.setPosition(glm::vec3( 0, 0, 15 ));
	camera.setTarget(glm::vec3( 0, 0, 0 ));
	//camera.setFOV( 60.0f );

	// polozenie zrodla swiatla
	LightPosition = glm::vec3(10,10,10);
}

// obsluga klawiatury, zmiana parametrow renderingu
void Game::Update()
{

	std::cout << Mouse::getPosition().x << std::endl;
	if (Mouse::getPosition().x > 410) {
		camera_rotate += 2;
	}
	else if (Mouse::getPosition().x < 390) {
		camera_rotate -= 2;
	}

	Mouse::resetPosition();

	theta += 0.02f;
	if(Keyboard::isPressed('W'))
	{
		LightPosition.z += 0.1f;
	}
	if(Keyboard::isPressed('S'))
	{
		LightPosition.z -= 0.1f;
	}
	if(Keyboard::isPressed('A'))
	{
		LightPosition.x -= 0.1f;
	}
	if(Keyboard::isPressed('D'))
	{
		LightPosition.x += 0.1f;
	}

	LightPosition.x = posX;
	LightPosition.y = posY;

	if (cooldown > 0)
		cooldown -= 1;

	if (Keyboard::isPressed('V'))
	{
		if (cooldown == 0) {
			bullets.push_back(new Bullet(posX, posY, rotate));
			cooldown = 15;
		}
	}

	if (camera_cooldown > 0)
		camera_cooldown -= 1;
	if (Keyboard::isPressed('N'))
	{
		if (camera_cooldown <= 0) {
			third_person = !third_person;
			camera_cooldown = 10;
			camera_rotate = 0;
		}
	}

	if (Keyboard::isPressed('P'))
	{
		camera_rotate++;
	}
	if (Keyboard::isPressed('O'))
	{
		camera_rotate--;
	}

	float oldPosX = posX;
	float oldPosY = posY;

	if(Keyboard::isPressed(GLFW_KEY_UP)) {
		posX = posX + speed * (float)cos( rotate * 3.14/180.0f );
		posY = posY - speed * (float)sin( rotate * 3.14/180.0f );
	}
	if(Keyboard::isPressed(GLFW_KEY_DOWN)) {
		posX = posX - speed * (float)cos( rotate * 3.14/180.0f );
		posY = posY + speed * (float)sin( rotate * 3.14/180.0f );
	}


	if(Keyboard::isPressed(GLFW_KEY_RIGHT)) {
		rotate += 2.f + speed;
	}
	if(Keyboard::isPressed(GLFW_KEY_LEFT)) {
		rotate -= 2.f + speed;
	}

	if(Keyboard::isPressed(GLFW_KEY_ESCAPE))
	{
		glfwSetWindowShouldClose(glfwGetCurrentContext(), GL_TRUE);
	}

	//std::cout << "X:" << posX << " Y:" << posY << std::endl;
	//std::cout << rotate << std::endl;
	
	for (int i = 0; i < bullets.size(); i++) {
		bullets.at(i)->updatePosition();
	}

	for (int i = 0; i < walls.size(); i++) {
		if (walls.at(i)->isCollision(posX, posY)) {
			posX = oldPosX;
			posY = oldPosY;
		}
		for (int j = 0; j < bullets.size(); j++) {
			if (walls.at(i)->isCollision(bullets.at(j)->x, bullets.at(j)->y)) {
				bullets.at(j)->hops--;
				if (bullets.at(j)->hops <= 0) {
					delete bullets.at(j);
					bullets.erase(bullets.begin() + j);
				}
				else {
					bullets.at(j)->rotate = 180 - bullets.at(j)->rotate;
				}
			}
		}
	}

	for (int i = 0; i < bad_boxes.size(); i++) {
		if (bad_boxes.at(i)->isCollision(posX, posY)) {
			posX = oldPosX;
			posY = oldPosY;
		}
		for (int j = 0; j < bullets.size(); j++) {
			if (bad_boxes.at(i)->isCollision(bullets.at(j)->x, bullets.at(j)->y)) {
				delete bullets.at(j);
				bullets.erase(bullets.begin() + j);
				delete bad_boxes.at(i);
				bad_boxes.erase(bad_boxes.begin() + i);
				break;
			}
		}
	}

	for (int i = 0; i < good_boxes.size(); i++) {
		if (good_boxes.at(i)->isCollision(posX, posY)) {
			delete good_boxes.at(i);
			good_boxes.erase(good_boxes.begin() + i);
			if (speed < 0.3) {
				speed += 0.01;
			}
		}
		for (int j = 0; j < bullets.size(); j++) {
			if (good_boxes.at(i)->isCollision(bullets.at(j)->x, bullets.at(j)->y)) {
				delete bullets.at(j);
				bullets.erase(bullets.begin() + j);
				delete good_boxes.at(i);
				good_boxes.erase(good_boxes.begin() + i);
				break;
			}
		}
	}


	// zmiana parametrow kamery w czasie ruchu
	// fp
	/*
	camera.setPosition(glm::vec3( posX, posY, 0.6 ));
	
	glm::mat4 camRot = glm::rotate(glm::mat4(1.0f), (rotate+90), glm::vec3( 0.0f, 0.0f, 1.0f));
	glm::vec4 up = camRot * glm::vec4(0,1,0,1);

	float upX = (float)cos((rotate) * 3.14 / 180);
	float upY = (float)sin((rotate) * 3.14 / 180);
	camera.setUp( glm::vec3(upX, -upY, 0) );

	float camTZ = 0;
	float camTY = posY - 3 * upY;
	float camTX = posX + 3 * upX;
	camera.setTarget(glm::vec3(camTX, camTY, camTZ));
	*/

	float upX = (float)cos((rotate + camera_rotate) * 3.14 / 180);
	float upY = (float)sin((rotate - camera_rotate) * 3.14 / 180);

	if (third_person)
		camera.setPosition(glm::vec3(posX - 5*upX, posY + 5*upY, 5));
	else
		camera.setPosition(glm::vec3(posX, posY, 0.6));

	glm::mat4 camRot = glm::rotate(glm::mat4(1.0f), (rotate + 90), glm::vec3(0.0f, 0.0f, 1.0f));
	glm::vec4 up = camRot * glm::vec4(0, 1, 0, 1);

	camera.setUp(glm::vec3(upX, -upY, 0));

	float camTZ = 0;
	float camTY = posY - 3.14 * upY;
	float camTX = posX + 3.14 * upX;
	camera.setTarget(glm::vec3(camTX, camTY, camTZ));

}


// rysowanie planszy gry skladajacej sie z obiektow sceneModelBox
void Game::drawScene( void ) {

	int i = 0;

	
	shaderColor->GetParameter("matColor")->SetValue(gameBox->GetMesh(i)->GetMaterial().Color);
	shaderColor->GetParameter("mode")->SetValue(-1.0f);

	for (int j = 0; j < bullets.size(); j++) {
		this->shaderColor->GetParameter("World")->SetValue(
			sceneModelBox->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(bullets.at(j)->x, bullets.at(j)->y, 0.2f)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::scale(glm::mat4(1.0f), glm::vec3(0.001f, 0.001f, 0.001f))
		);
		sceneModelBox->GetMesh(i)->Draw();
	}

	for (int j = 0; j < bad_boxes.size(); j++) {
		this->shaderColor->GetParameter("World")->SetValue(
			sceneModelBox->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(bad_boxes.at(j)->position.x, bad_boxes.at(j)->position.y, bad_boxes.at(j)->position.z)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::scale(glm::mat4(1.0f), glm::vec3(bad_boxes.at(j)->scale.width, bad_boxes.at(j)->scale.height, bad_boxes.at(j)->scale.length))
		);
		sceneModelBox->GetMesh(i)->Draw();
	}

	shaderColor->GetParameter("matColor")->SetValue(sceneModelBox->GetMesh(i)->GetMaterial().Color);

	// rysowanie scian
	for (int j = 0; j < walls.size(); j++) {
		this->shaderColor->GetParameter("World")->SetValue(
			sceneModelBox->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(walls.at(j)->position.x, walls.at(j)->position.y, walls.at(j)->position.z)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::scale(glm::mat4(1.0f), glm::vec3(walls.at(j)->scale.width, walls.at(j)->scale.height, walls.at(j)->scale.length))
		);
		sceneModelBox->GetMesh(i)->Draw();
	}

	// rysowanie podlogi
	this->shaderColor->GetParameter("World")->SetValue(
		sceneModelBox->GetMesh(i)->getLocalWorld() *
		glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0)) *
		glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
		glm::scale(glm::mat4(1.0f), glm::vec3(20, 0, 30))
	);
	sceneModelBox->GetMesh(i)->Draw();

	// rysowanie good boxow
	for (int j = 0; j < good_boxes.size(); j++) {
		this->shaderColor->GetParameter("World")->SetValue(
			sceneModelBox->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(good_boxes.at(j)->position.x, good_boxes.at(j)->position.y, good_boxes.at(j)->position.z)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *
			glm::scale(glm::mat4(1.0f), glm::vec3(good_boxes.at(j)->scale.width, good_boxes.at(j)->scale.height, good_boxes.at(j)->scale.length))
		);
		sceneModelBox->GetMesh(i)->Draw();
	}
}


// rysowanie sceny (glowna petla)
void Game::Redraw()
{
	//ustawienie textury do ktorej chcemy renderowa�
	rt->SetRenderTarget();
	glClearColor(0.6f, 0.5f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	//ustawienie shadera e jako aktywnego
	shaderColor->Apply();
	//ustawienie macierzy widoku i projekcji
	shaderColor->GetParameter("View")->SetValue(camera.getView());
	shaderColor->GetParameter("Projection")->SetValue(camera.getProjection());
	//ustawienie pozycji �wiat�a i camery
	shaderColor->GetParameter("LightPosition")->SetValue(LightPosition);
	shaderColor->GetParameter("EyePosition")->SetValue(camera.getPosition());

	int i = 0;
	// rysowanie diamentu
	if (diamond->GetMesh(i)->GetMaterial().texture != NULL)
	{
		//je�li mamy tekstur� to j� bindujemy i ustawiamy tryb teskturowania w shaderze
		shaderColor->GetParameter("mode")->SetValue(1.0f);
		shaderColor->GetParameter("tex")->SetValue(*diamond->GetMesh(i)->GetMaterial().texture);
	}
	else
	{
		//w��czenie trybu bez tekstury
		shaderColor->GetParameter("mode")->SetValue(-1.0f);
	}

	for (int i = 0; i < diamond->Elements(); i++) {

		shaderColor->GetParameter("World")->SetValue(

			diamond->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(0, 20, 1)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *

			glm::scale(glm::mat4(1.0f), glm::vec3(0.1f, 0.1f, 0.1f))
		);

		// rysowanie modelu
		diamond->GetMesh(i)->Draw();
	}


	//rysowanie samolotu
	if (samolot->GetMesh(i)->GetMaterial().texture != NULL)
	{
		//je�li mamy tekstur� to j� bindujemy i ustawiamy tryb teskturowania w shaderze
		shaderColor->GetParameter("mode")->SetValue(1.0f);
		shaderColor->GetParameter("tex")->SetValue(*samolot->GetMesh(i)->GetMaterial().texture);
	}
	else
	{
		//w��czenie trybu bez tekstury
		shaderColor->GetParameter("mode")->SetValue(-1.0f);
	}

	for (int i = 0; i < samolot->Elements(); i++) {

		shaderColor->GetParameter("World")->SetValue(

			samolot->GetMesh(i)->getLocalWorld() *
			glm::translate(glm::mat4(1.0f), glm::vec3(0, 70, 0)) *
			glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3(1.0f, 0.0f, 0.0f)) *

			glm::scale(glm::mat4(1.0f), glm::vec3(0.1, 0.1, 0.1))
		);

		// rysowanie modelu
		samolot->GetMesh(i)->Draw();
	}
	


	shaderColor->GetParameter("matColor")->SetValue(testModel->GetMesh(i)->GetMaterial().Color);
	if(testModel->GetMesh(i)->GetMaterial().texture != NULL)
	{
		//je�li mamy tekstur� to j� bindujemy i ustawiamy tryb teskturowania w shaderze
		shaderColor->GetParameter("mode")->SetValue(1.0f);
		shaderColor->GetParameter("tex")->SetValue(*testModel->GetMesh(i)->GetMaterial().texture);
	}
	else
	{
		//w��czenie trybu bez tekstury
		shaderColor->GetParameter("mode")->SetValue(-1.0f);
	}

	// ustawienie transformacji obiektu
	// model mo�e si� sk�ada� z kilku cz�ci (meshy) kt�re znajduj� si� w przestrzeni lokalnej modelu,
	// musimy skorygowa� ich pozycj� i przekszta�ci� do przestrzeni naszego �wiata,
	// w tym celu wyci�gamy macierz transformacji mesha testModel->GetMesh(i)->getLocalWorld(),
	// a nast�pnie przekszta�camy w podobny spos�b jak w starszych wersjach OpenGL

	for(int i = 0; i < testModel->Elements(); i++) {

		shaderColor->GetParameter("World")->SetValue(
			
			testModel->GetMesh(i)->getLocalWorld() *
				glm::translate(glm::mat4(1.0f), glm::vec3( posX, posY, 0 )) *
				glm::rotate(glm::mat4(1.0f), -rotate, glm::vec3( 0.0f, 0.0f, 1.0f )) *
				glm::rotate(glm::mat4(1.0f), 90.0f, glm::vec3( 1.0f, 0.0f, 0.0f )) *
		
				glm::scale(glm::mat4(1.0f), glm::vec3( 0.1, 0.1, 0.1 ))
				);

		// rysowanie modelu
		testModel->GetMesh(i)->Draw();
	}

	// rysowanie sceny
	drawScene();


	// mapowanie tonow za pomoca drugiego shadera, wynik renderowany jest do drugiego render targetu (rtTMO)
	rtTMO->SetRenderTarget();
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shaderTMO->Apply();
	shaderTMO->GetParameter("tex")->SetValue(*rt);
	shaderTMO->GetParameter("gamma")->SetValue(1.6f);
	quad->Draw(0, 0, 800, 600, shaderTMO->GetParameter("World"));
		
	//rendering na ekran (do framebuffera)
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, 800, 600);
	glClearColor(0.0, 0.0, 0.0, 0.0); //zmiana glClearColor by sprawdzi� czy faktycznie wyrenderowali�my co� do tekstury
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	shaderTextureDrawer->Apply();
	quad->Draw(0, 0, 800, 600, shaderTextureDrawer->GetParameter("World"));

}



