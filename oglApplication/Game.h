#pragma once
#include "Effect.h"
#include "Model.h"
#include "Texture2D.h"
#include "Camera.hpp"
#include "RenderTarget2D.h"
#include "Quad.h"
#include "Box.h"
#include "Utils.h"
#include "Bullet.h"
#include "3rd\GLFW\include\glfw3.h"

using namespace std;

class Game {
	Quad* quad;
	Texture2D* tex;
	RenderTarget2D* rtTMO;
	RenderTarget2D* rt;

	bool third_person;

	Model* testModel;
	float posX;
	float posY;
	float rotate;
	float speed;
	float camera_rotate;

	Model* samolot;
	Texture2D* samolot_tex;

	Model* diamond;
	Texture2D* diamond_tex;

	Model* sceneModelBox;
	Model* gameBox;
	int sceneWidth;
	int sceneHeight;
	int cooldown;
	int camera_cooldown;
	

	// moje obiekty
	std::vector<Box*> walls;
	std::vector<Box*> good_boxes;
	std::vector<Box*> bad_boxes;
	std::vector<Bullet*> bullets;

	Effect* shaderColor;
	Effect* shaderTMO;
	Effect* shaderTextureDrawer;
	Camera camera;
	glm::vec3 LightPosition;
	float theta;
public:
	Game(void);
	~Game(void);
	void Update();
	void Init();
	void Redraw();
	void Input();
	void drawScene( void );
};

